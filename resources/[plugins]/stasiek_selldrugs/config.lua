Config                            = {}
Config.Locale = 'en'
Config.TimeToSell = 5 -- how many seconds player have to wait/stand near ped
Config.CallCops = true -- if true and if ped reject your offer then there is [Config.CallCopsPercent]% to call cops
Config.CopsRequiredToSell = 1 -- required cops on server to sell drugs
Config.CallCopsPercent = 1 -- (min1) if 1 cops will be called every time=100%, 2=50%, 3=33%, 4=25%, 5=20%
Config.PedRejectPercent = 3 -- (min1) if 1 ped reject offer=100%, 2=50%, 3=33%, 4=25%, 5=20%
Config.PlayAnimation = true -- just play animation when sold
Config.SellPooch = true -- if true, players can sell pooch like weed_pooch, meth_pooch
Config.SellSingle = false -- if true, players can sell single item like weed, meth
Config.SellWeed = true	-- if true, players can sell weed
Config.SellMeth = true	-- if true, players can sell meth
Config.SellCoke = true	-- if true, players can sell coke
Config.SellOpiu = true	-- if true, players can sell opium
Config.WeedPrice = 150	-- sell price for single, not pooch (black money)
Config.MethPrice = 300	-- sell price for single, not pooch (black money)
Config.CokePrice = 500	-- sell price for single, not pooch (black money)
Config.OpiuPrice = 200	-- sell price for single, not pooch (black money)
Config.DistanceFromCity = 10000 -- set distance that player cant sell drugs too far from city
Config.CityPoint = {x= -255.94, y= -983.93, z= 30.21}
