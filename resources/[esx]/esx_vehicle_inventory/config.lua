--Truck
Config	=	{}

Config.green                   = 56108
Config.grey                   = 8421504
Config.red                       = 16711680
Config.orange                   = 16744192
Config.blue                   = 2061822
Config.purple                   = 11750815
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"

 -- Limit, unit can be whatever you want. Originally grams (as average people can hold 25kg)
Config.Limit = 25000

-- Default weight for an item:
	-- weight == 0 : The item do not affect character inventory weight
	-- weight > 0 : The item cost place on inventory
	-- weight < 0 : The item add place on inventory. Smart people will love it.
Config.DefaultWeight = 2000



-- If true, ignore rest of file
Config.WeightSqlBased = false

-- I Prefer to edit weight on the config.lua and I have switched Config.WeightSqlBased to false:

Config.localWeight = {
 black_money = 1,
 WEAPON_PISTOL = 1500,
 bread = 75,  		--30
 water = 150,		--30
 fish = 135,
 stone = 1920,
 washed_stone = 1920,
 copper = 250,
 iron = 332, 
 gold = 665,
 diamond = 1000,
 wood = 170,
 cutted_wood = 170,
 packaged_plank = 135,
 petrol = 562,
 petrol_raffin = 562,
 essence = 562,
 wool = 337,
 fabric = 168,
 clothe = 337,
 gazbottle = 2000,	
 fixtool = 500,		
 carotool = 900,	
 blowpipe = 1200,	
 fixkit = 500,		
 carokit = 500,		
 bandage = 50,		
 medikit = 300,		
 weed = 300,			
 weed_pooch = 900,	
 meth = 300,			
 meth_pooch = 900,	
 coke = 300,			
 coke_pooch = 900,	
 opium = 300,		
 opium_pooch = 900,	
 croquettes = 50,	
 raisin = 400,		
 jus_raisin = 400,	
 vine = 400,		 
 grand_cru = 400,	
 jusfruit = 600,	
 clip = 1000,		
 cola = 500,		
 brolly = 200,		
 bong = 500,		
 rose = 10,			
 notepad = 10,		
 icetea = 500,		
 grapperaisin = 50, 
 alive_chicken = 675, 
 slaughtered_chicken = 675, 
 packaged_chicken =135,
}

Config.VehicleLimit ={
    [0]=13500,
    [1]=13500,
    [2]=27000,
    [3]=13500,
    [4]=13500,
    [5]=13500,
    [6]=13500,
    [7]=13500,
    [8]=6750,
    [9]=40500,
    [10]=40500,
    [11]=40500,
    [12]=40500,
    [13]=0,
    [14]=40500,
    [15]=40500,
    [16]=40500,
    [17]=40500,
    [18]=40500,
    [19]=40500,
    [20]=40500,
}
