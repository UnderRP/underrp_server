Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false
Config.EnableESXIdentity          = true -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 5000 },
      { name = 'WEAPON_STUNGUN',          price = 15000 },
    },

	  AuthorizedVehicles = {
		  { name = 'flatbed',  label = 'Laweta' },
	  },

    Cloakrooms = {
      { x = 1009.91, y = -3168.42, z = -39.86 },
    },

    Armories = {
      { x = 1008.43, y = -3171.48, z = -39.85 },
    },

    Vehicles = {
      {
        Spawner    = { x = -66.11, y = 84.84, z = 70.56 },
        SpawnPoint = { x = -63.63, y = 89.72, z = 73.55 },
        Heading    = 70.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -74.92, y = 94.29, z = 72.08 },
    },

    BossActions = {
      { x = 1009.17, y = -3164.75, z = -35.03 }
    },

  },

}