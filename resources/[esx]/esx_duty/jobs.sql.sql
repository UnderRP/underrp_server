INSERT INTO `jobs` (name, label) VALUES
  ('offmecano','Status4')
;

UPDATE `job_grades` SET `grade` = '9', `salary` = '2500' WHERE `job_grades`.`name` = 'boss' and `job_grades`.`job_name` = 'police';
UPDATE `job_grades` SET `grade` = '9', `salary` = '250' WHERE `job_grades`.`name` = 'boss' and `job_grades`.`job_name` = 'offpolice';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Starszy Posterunkowy' WHERE `job_grades`.`name` = 'sergeant' and `job_grades`.`job_name` = 'police';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Starszy Posterunkowy' WHERE `job_grades`.`name` = 'sergeant' and `job_grades`.`job_name` = 'offpolice';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Przodownik' WHERE `job_grades`.`name` = 'lieutenant' and `job_grades`.`job_name` = 'police';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Przodownik' WHERE `job_grades`.`name` = 'lieutenant' and `job_grades`.`job_name` = 'offpolice';

UPDATE `job_grades` SET `grade` = '9', `salary` = '2500' WHERE `job_grades`.`name` = 'boss' and `job_grades`.`job_name` = 'ambulance';
UPDATE `job_grades` SET `grade` = '9', `salary` = '250' WHERE `job_grades`.`name` = 'boss' and `job_grades`.`job_name` = 'offambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Rekrut' WHERE `job_grades`.`name` = 'ambulance' and `job_grades`.`job_name` = 'ambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Rekrut' WHERE `job_grades`.`name` = 'ambulance' and `job_grades`.`job_name` = 'offambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Kadet' WHERE `job_grades`.`name` = 'doctor' and `job_grades`.`job_name` = 'ambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Kadet' WHERE `job_grades`.`name` = 'doctor' and `job_grades`.`job_name` = 'offambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Sanitariusz' WHERE `job_grades`.`name` = 'chief_doctor' and `job_grades`.`job_name` = 'ambulance';
UPDATE `job_grades` SET `salary` = '250', `label` = 'Sanitariusz' WHERE `job_grades`.`name` = 'chief_doctor' and `job_grades`.`job_name` = 'offambulance';

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('police',4,'aspirant','Aspirant',250,'{}','{}'),
  ('offpolice',4,'aspirant','Aspirant',250,'{}','{}'),
  ('police',5,'staspirant','Starszy Aspirant',250,'{}','{}'),
  ('offpolice',5,'staspirant','Starszy Aspirant',250,'{}','{}'),
  ('police',6,'inspektor','Inspektor',250,'{}','{}'),
  ('offpolice',6,'inspektor','Inspektor',250,'{}','{}'),
  ('police',7,'inspektorgen','Inspektor Generalny',250,'{}','{}'),
  ('offpolice',7,'inspektorgen','Inspektor Generalny',250,'{}','{}'),
  ('police',8,'swat','SWAT',250,'{}','{}'),
  ('offpolice',8,'swat','SWAT',250,'{}','{}'),
  ('ambulance',4,'ratownik','Ratownik',250,'{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}','{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
  ('offambulance',4,'ratownik','Ratownik',250,'{}','{}'),
  ('ambulance',5,'lekarz','Lekarz',250,'{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}','{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
  ('offambulance',5,'lekarz','Lekarz',250,'{}','{}'),
  ('ambulance',6,'speclekarz','Lekarz spec.',250,'{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}','{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
  ('offambulance',6,'speclekarz','Lekarz spec.',250,'{}','{}'),
  ('ambulance',7,'chirurg','Chirurg',250,'{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}','{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
  ('offambulance',7,'chirurg','Chirurg',250,'{}','{}'),
  ('ambulance',8,'dyro','Dyrektor',250,'{"tshirt_2":0,"hair_color_1":5,"glasses_2":3,"shoes":9,"torso_2":3,"hair_color_2":0,"pants_1":24,"glasses_1":4,"hair_1":2,"sex":0,"decals_2":0,"tshirt_1":15,"helmet_1":8,"helmet_2":0,"arms":92,"face":19,"decals_1":60,"torso_1":13,"hair_2":0,"skin":34,"pants_2":5}','{"tshirt_2":3,"decals_2":0,"glasses":0,"hair_1":2,"torso_1":73,"shoes":1,"hair_color_2":0,"glasses_1":19,"skin":13,"face":6,"pants_2":5,"tshirt_1":75,"pants_1":37,"helmet_1":57,"torso_2":0,"arms":14,"sex":1,"glasses_2":0,"decals_1":0,"hair_2":0,"helmet_2":0,"hair_color_1":0}'),
  ('offambulance',8,'dyro','Dyrektor',250,'{}','{}'),
  ('offmecano',0,'recrue','Świerzak',250,'{}','{}'),
  ('offmecano',1,'novice','Nowicjusz',250,'{}','{}'),
  ('offmecano',2,'experimente','Doświadczony',250,'{}','{}'),
  ('offmecano',3,'chief','Szef',250,'{}','{}'),
  ('offmecano',4,'boss','Prezes',250,'{}','{}')
;
