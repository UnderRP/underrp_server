Locales['en'] = {
  -- regulars
  	['duty'] = 'Wciśnij ~INPUT_CONTEXT~ aby ~g~wejść~s~ lub ~r~wyjść~s~ ze służby',
	['onduty'] = 'Jesteś na służbie.',
	['offduty'] = 'Masz wolne.',
	['notpol'] = 'Nie jesteś policjantem.',
	['notamb'] = 'Nie jesteś medykiem.',
	['notmec'] = 'Nie jesteś mechanikiem.',
}
