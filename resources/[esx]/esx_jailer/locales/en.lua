Locales ['en'] = {
	['blip_name']          = 'Wiezienie',
	['judge']              = 'SEDZIA',
	['escape_attempt']     = 'Nie możesz uciec z wiezienia!',
	['remaining_msg']      = 'Zostało ~b~%s~s~ sekund odsiadki',
	['jailed_msg']         = '%s jest w wiezieniu na %s miesiecy',
	['unjailed']           = '%s wyszedl z wiezienia!',
        ['jail_menu_info']      = 'Wiezienie'
}
