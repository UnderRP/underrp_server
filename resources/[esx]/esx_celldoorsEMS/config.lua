Config = {}
Config.Locale = 'en'

Config.DoorList = {
	--szpital
	--operacyjne 1 lewe
	{
		objName = 'v_ilev_cor_doorglassb',
		objCoords  = {x = 252.690, y = -1361.460, z = 24.680},
		textCoords = {x = 253.140, y = -1361.030, z = 25.000},
		locked = true,
		distance = 3
	},
	--operacyjne 1 prawe
	{
		objName = 'v_ilev_cor_doorglassa',
		objCoords  = {x = 254.340, y = -1359.490, z = 24.680},
		textCoords = {x = 253.970, y = -1360.010, z = 25.000},
		locked = true,
		distance = 3
	},
	--operacyjne 2 lewe
	{
		objName = 'v_ilev_cor_doorglassb',
		objCoords  = {x = 265.77, y = -1345.87, z = 24.680},
		textCoords = {x = 266.23, y = -1345.4, z = 25.000},
		locked = true,
		distance = 3
	},
	--operacyjne 2 prawe
	{
		objName = 'v_ilev_cor_doorglassa',
		objCoords  = {x = 267.42, y = -1343.90, z = 24.680},
		textCoords = {x = 267.02, y = -1344.42, z = 25.000},
		locked = true,
		distance = 3
	},
	--operacyjne wewnątrz lewe
	{
		objName = 'v_ilev_cor_doorglassa',
		objCoords  = {x = 257.30, y = -1348.85, z = 24.680},
		textCoords = {x = 256.80, y = -1348.42, z = 25.000},
		locked = true,
		distance = 3
	},
	--operacyjne wewnątrz prawe
	{
		objName = 'v_ilev_cor_doorglassb',
		objCoords  = {x = 255.33, y = -1347.20, z = 24.680},
		textCoords = {x = 255.74, y = -1347.59, z = 25.000},
		locked = true,
		distance = 3
	},
	
	--operacyjne 3 lewe
	{
		objName = 'v_ilev_cor_doorglassb',
		objCoords  = {x = 281.93, y = -1342.91, z = 24.680},
		textCoords = {x = 282.34, y = -1342.4, z = 25.000},
		locked = true,
		distance = 2
	},
	--operacyjne 3 prawe
	{
		objName = 'v_ilev_cor_doorglassa',
		objCoords  = {x = 283.59, y = -1340.94, z = 24.680},
		textCoords = {x = 283.18, y = -1341.41, z = 25.000},
		locked = true,
		distance = 2
	},
	
	--kostnica lewe
	{
		objName = 'v_ilev_cor_doorglassb',
		objCoords  = {x = 287.24, y = -1343.99, z = 24.680},
		textCoords = {x = 286.73, y = -1344.48, z = 25.000},
		locked = true,
		distance = 3
	},
	--kostnica prawe
	{
		objName = 'v_ilev_cor_doorglassa',
		objCoords  = {x = 285.58, y = -1345.97, z = 24.680},
		textCoords = {x = 285.95, y = -1345.44, z = 25.000},
		locked = true,
		distance = 3
	},
		--recepcja
	{
		objName = 'v_ilev_cor_firedoorwide',
		objCoords  = {x = 272.22, y = -1361.57, z = 24.550},
		textCoords = {x = 271.75, y = -1361.2, z = 25.000},
		locked = true,
		distance = 3
	},
		--labolatorium lewe
	{
		objName = 'v_ilev_cor_firedoor',
		objCoords  = {x = 252.88, y = -1366.76, z = 39.55},
		textCoords = {x = 252.43, y = -1366.49, z = 40.000},
		locked = true,
		distance = 3
	},
		--labolatorium prawe
	{
		objName = 'v_ilev_cor_firedoor',
		objCoords  = {x = 251.11, y = -1365.28, z = 39.55},
		textCoords = {x = 251.48, y = -1365.73, z = 40.000},
		locked = true,
		distance = 3
	},
	
}