local blips = {
    -- Example {title="", colour=, id=, x=, y=, z=},
	 {title="Psycholog", colour=3, id=498, x = -1894.84, y = -567.48, z = 11.81}, -- off
         {title="Sąd", colour=5, id=206, x = 224.49, y = -442.6, z = 45.25},
         {title="Prawnik", colour=3, id=76, x = -114.27, y = -607.19, z = 36.28}
 }
      
Citizen.CreateThread(function()

    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, 1.0)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)