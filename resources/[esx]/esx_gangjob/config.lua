Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = false
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.GangStations = {

  Gang = {

    Blip = {
--      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 9 },
      { name = 'WEAPON_COMBATPISTOL',     price = 3 },
     { name = 'WEAPON_ASSAULTSMG',       price = 1 },
      { name = 'WEAPON_ASSAULTRIFLE',     price = 15 },
      { name = 'WEAPON_PUMPSHOTGUN',      price = 6 },
      { name = 'WEAPON_STUNGUN',          price = 5 },
     -- { name = 'WEAPON_FLASHLIGHT',       price = 800 },
     -- { name = 'WEAPON_FIREEXTINGUISHER', price = 1200 },
     -- { name = 'WEAPON_FLAREGUN',         price = 6000 },
      { name = 'GADGET_PARACHUTE',        price = 3 },
	  { name = 'WEAPON_BAT'		,        price = 3 },
     -- { name = 'WEAPON_STICKYBOMB',       price = 200000 },
      { name = 'WEAPON_SNIPERRIFLE',      price = 22 },
      { name = 'WEAPON_SPECIALCARBINE',         price = 3 },
      { name = 'WEAPON_CARBINERIFLE',          price = 1 },
     -- { name = 'WEAPON_BZGAS',            price = 120000 },
    --  { name = 'WEAPON_SMOKEGRENADE',     price = 100000 },
     -- { name = 'WEAPON_APPISTOL',         price = 70000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 11 },
      { name = 'WEAPON_HEAVYSNIPER',      price = 20 },
    --  { name = 'WEAPON_MINIGUN',          price = 700000 },
     -- { name = 'WEAPON_RAILGUN',          price = 2500000 },
    },

	  AuthorizedVehicles = {
		  { name = 'RHINO',  label = 'Czolg' },
		  { name = 'Dune3',    label = 'Lazik wojskowy' },
		  { name = 'Barrage',   label = '2 dzialkowiec' },
		  { name = 'Brickade',   label = 'Transportowiec broni/brudnej' },
		  { name = 'Hunter',   label = 'Helikopter operacjyny' },
		  { name = 'Akula',   label = 'Helikopter Nocny' },
		  { name = 'Savage',   label = 'Helikopter bojowy' },
		  { name = 'Cargobob',   label = 'Transportowiec pojazdow/zolniezy' },
		  { name = 'Buzzard2',   label = 'Transportowiec zolniezy maly' },
		  { name = 'Strikeforce',   label = 'Mysliwiec odrzutowy' },
		  { name = 'Mogul',   label = 'Transporter zolniezy duzy' },
		  { name = 'Nokota',   label = 'Mysliwiec' },
		  { name = 'Bombushka',   label = 'Duzy samolot zwiadowczo/transportowy' },
		  { name = 'Avenger',   label = 'Samoloto/helikopter' },
		  { name = 'APC',   label = 'APC' },
	  	  { name = 'Valkyrie',   label = 'Smiglowiec transportowy' },
		  { name = 'Chernobog',   label = 'Altyreria' },
		  { name = 'Barracks',   label = 'Cienzarowka wojskowa' },
		  { name = 'Crusader',   label = 'Pojazd zwykly wojskowy' },
		  { name = 'Volatol',   label = 'duzy odzutowiec transportowy' },
		  { name = 'Titan',   label = 'Samolot transportowy ogromny Titan' },
		  { name = 'Rogue',   label = 'samolot zwiadowcy' },
		  { name = 'Tula',   label = 'Samolot operacyjny' },
		  { name = 'mule3',      label = 'Camion de Transport' },
	  },

    Cloakrooms = {
      { x = -929.05908203125, y = -2937.634765625, z = 13.945072174072},
    },

    Armories = {
      { x = -940.21026611328, y = -2962.9470214844, z = 13.945075035096},
    },

    Vehicles = {
      {
        Spawner    = { x = -940.91400146484, y = -2955.8583984375, z = 13.945076942444 },
        SpawnPoint = { x = -970.00903320312, y = -3000.0749511718, z = 13.945080757142 },
        Heading    = 261.51,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 113.30500793457, y = -3109.3337402344, z = 5.0060696601868 },
        SpawnPoint = { x = 112.94457244873, y = -3102.5942382813, z = 5.0050659179688 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -975.84295654296, y = -2955.1884765625, z = 13.945075035096 },
      
    },

    BossActions = {
      { x = -938.30645751954, y = -938.30645751954, z = -2930.9931640625 },
    },

  },

}
