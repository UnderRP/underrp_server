Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 0, g = 250, b = 0 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.DismayStations = {

  Dismay = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
         { name = 'WEAPON_COMBATPISTOL',     	     price = 25000 },
--	 { name = 'WEAPON_VINTAGEPISTOL',     price = 60000 },
--	 { name = 'WEAPON_SMG',       price = 150000 },
--	 { name = 'WEAPON_ASSAULTRIFLE',       price = 400000 },
--	 { name = 'WEAPON_MICROSMG',       price = 125000 },
 --    { name = 'WEAPON_STUNGUN',          price = 4000 },
 --    { name = 'WEAPON_FLASHLIGHT',       price = 500 },
--	 { name = 'WEAPON_BAT'		,        price = 500 },
--	 { name = 'WEAPON_SWITCHBLADE',      price = 500 },
--	 { name = 'WEAPON_KNIFE',            price = 500 },
--	 { name = 'WEAPON_HATCHET',          price = 500 },
--	 { name = 'WEAPON_crowbar',          price = 500 },

	  
    },

	  AuthorizedVehicles = {
	--	  { name = 'pony',  label = 'Pony' },
	--	  { name = 'burrito2',      label = 'Burrito' },
	--	  { name = 'boxville2',      label = 'boxville' },
	--	  { name = 'guardian',   label = 'Grand 4x4' },
	--	  { name = 'burrito3',   label = 'Fourgonnette' },
	--	  { name = 'cog55',       label = 'Executive' },
	--	  { name = 'mesa',       label = 'Tout-Terrain' },
	  },

    Cloakrooms = {
      { x = -811.629, y = 175.177, z = -76.745 },
    },

    Armories = {
      { x = 251.12, y = -288.56, z = -1.9 },
    },

    Vehicles = {
      {
        Spawner    = { x = 1551.78, y = 2193.05, z =  78.055 },
        SpawnPoint = { x = 1549.85, y = 2202.99, z = 78.74 },
        Heading    = 82.5,
      }
    },
	
	Helicopters = {
      {
            Spawner    = { x = 328.31759643555, y = -2018.2722167969, z = -21.705713272095 },
        SpawnPoint = { x = 318.77709960938, y = -2027.2596435547, z = -20.674409866333 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
    --  { x = -806.189, y = 162.638, z = 71.541 },
      { x = 1542.39, y = 2186.41, z = 78.00 },
    },

    BossActions = {
      { x = 263.52, y = -292.18, z = -1.7 }
    },

  },

}

Config.green 				  = 681014
Config.grey 				  = 8421504
Config.red 					  = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.yelow				  = 15921158
Config.purple 				  = 11750815
Config.phone 				  = 12654922
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"