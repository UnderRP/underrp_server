
Config = {
        Locale = 'en',
	DrawDistance = 100,
	Price = 1500,
	BlipInfos = {
		Sprite = 357,
		Color = 3 
	},
	BlipPound = {
		Sprite = 67,
		Color = 64 
	}
}
Config.RequiredMecs = 0

Config.Garages = {
	Garage_Centre = {	
		Pos = {x=39.55, y=-872.43, z=30.727},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=25.9, y= -878.7, z= 30.23},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading=157.84,
			Marker = 36		
		},
		DeletePoint = {
			Pos = {x=44.48, y= -886.65, z= 30.23},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			
		},
		MunicipalPoundPoint = {
			Pos = {x=482.896, y=-1316.557, z=30.301},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
		SpawnMunicipalPoundPoint = {
			Pos = {x=490.942, y=-1313.067, z=28.964},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			Heading=299.42
		},
	},
	Garage_Paleto = {	
		Pos = {x=105.359, y=6613.586, z=32.3973},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=128.7822, y= 6622.9965, z= 31.7828},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
		DeletePoint = {
			Pos = {x=126.3572, y=6608.4150, z=31.8565},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
		MunicipalPoundPoint = {
			Pos = {x=-185.187, y=6272.027, z=31.580},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
		SpawnMunicipalPoundPoint = {
			Pos = {x=-199.160, y=6274.180, z=31.580},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
	},
	
	Garage_Sandy = {	
		Pos = {x=1705.80, y=3795.83, z=35.38},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=1712.31, y=3772.93, z=35.42},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading=323.66,
			Marker = 36		
		},
		DeletePoint = {
			Pos = {x=1711.26, y=3761.32, z=35.20},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			
		},
		MunicipalPoundPoint = {
			Pos = {x=1735.70, y=3730.63, z=34.92},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
		SpawnMunicipalPoundPoint = {
			Pos = {x=1722.26, y=3718.83, z=35.13},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			Heading=75.27
		},
	},

	Garage_Melina = {	
		Pos = {x=-1230.49, y=-1225.25, z=6.9},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=-1230.49, y=-1225.25, z=6.9},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading = 108.4,
			Marker = 36
		},
		DeletePoint = {
			Pos = {x=-1243.6, y=-1229.48, z=7.06},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
                MunicipalPoundPoint = {
			Pos = {x=-185.187, y=6272.027, z=31.580},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
		SpawnMunicipalPoundPoint = {
			Pos = {x=-199.160, y=6274.180, z=31.580},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	

          },
	Garage_Czerwony = {	
		Pos = {x=-347.42, y=-754.42, z=33.97},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=-347.42, y=-754.42, z=33.97},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading = 53.6,
			Marker = 36
		},
		DeletePoint = {
			Pos = {x=-347.58, y=-764.48, z=33.97},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
                MunicipalPoundPoint = {
			Pos = {x=-185.187, y=6272.027, z=31.580},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
		SpawnMunicipalPoundPoint = {
			Pos = {x=-199.160, y=6274.180, z=31.580},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
},
    	Garage_Wiezienie = {	
		Pos = {x=1876.45, y=2618.72, z=45.67},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=1876.45, y=2618.72, z=45.67},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading = 53.6,
			Marker = 36
		},
		DeletePoint = {
			Pos = {x=1870.25, y=2618.87, z=45.67},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
                MunicipalPoundPoint = {
			Pos = {x=-185.187, y=6272.027, z=31.580},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
		SpawnMunicipalPoundPoint = {
			Pos = {x=-199.160, y=6274.180, z=31.580},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
},
    	Garage_Adwokat = {	
		Pos = {x=1876.45, y=2618.72, z=45.67},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 0,
		SpawnPoint = {
			Pos = {x=-149.66, y=-616.47, z=33.42},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading = 53.6,
			Marker = 36
		},
		DeletePoint = {
			Pos = {x=-172, y=-606.24, z=33.42},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
                MunicipalPoundPoint = {
			Pos = {x=-185.187, y=6272.027, z=31.580},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
		SpawnMunicipalPoundPoint = {
			Pos = {x=-199.160, y=6274.180, z=31.580},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},	
},
		Garage_Szeris = {	
			Pos = {x=-103.93, y=89.8, z=71.62},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Color = {r = 204, g = 204, b = 0},
			Marker = 36,
					Show = 1,
			SpawnPoint = {
				Pos = {x=-94.21, y=83.32, z=71.64},
				Color = {r=0,g=255,b=0},
				Size  = {x = 3.0, y = 3.0, z = 2.0},
				Heading = 53.6,
				Marker = 36
			},
			DeletePoint = {
				Pos = {x=-88.98, y=93.32, z=72.31},
				Color = {r=255,g=0,b=0},
				Size  = {x = 3.0, y = 3.0, z = 2.0},
				Marker = 36
			},
					MunicipalPoundPoint = {
				Pos = {x=-185.187, y=6272.027, z=31.580},
				Color = {r=25,g=25,b=112},
				Size  = {x = 3.0, y = 3.0, z = 2.0},
				Marker = 36
			},	
			SpawnMunicipalPoundPoint = {
				Pos = {x=-199.160, y=6274.180, z=31.580},
				Color = {r=0,g=255,b=0},
				Size  = {x = 3.0, y = 3.0, z = 2.0},
				Marker = 36
			},	
},
			Garage_Inny = {	
		Pos = {x=224.88, y=-788.18, z=30.87},
		Size  = {x = 3.0, y = 3.0, z = 2.0},
		Color = {r = 204, g = 204, b = 0},
		Marker = 36,
                Show = 1,
		SpawnPoint = {
			Pos = {x=232.66, y= -791.92, z= 30.59},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Heading=157.84,
			Marker = 36		
		},
		DeletePoint = {
			Pos = {x=216.29, y= -785.73, z= 30.82},
			Color = {r=255,g=0,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			
		},
		MunicipalPoundPoint = {
			Pos = {x=125.13, y=-1081.08, z=29.19},
			Color = {r=25,g=25,b=112},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36
		},
		SpawnMunicipalPoundPoint = {
			Pos = {x=117.0, y=-1074.94, z=29.19},
			Color = {r=0,g=255,b=0},
			Size  = {x = 3.0, y = 3.0, z = 2.0},
			Marker = 36,
			Heading=299.42
		},
	},
}
-- /tp 229.700 -800.1149 29.5722