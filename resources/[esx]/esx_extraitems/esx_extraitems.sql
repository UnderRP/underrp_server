INSERT INTO `items` (`name`, `label`, `limit`) VALUES
	('drill', 'Drill', 1, 0, 1),
	('oxygen_mask', 'Oxygen Mask', -1, 0, 1),
	('bulletproof', 'Bullet-Proof Vest', -1, 0, 1),
	('clip', 'Weapon Clip', -1, 0, 1),
	('grip', 'Grip', -1, 0, 1),
	('flashlight', 'Flashlight', -1, 0, 1),
	('silent', 'Silencer', -1, 0, 1),
	('scope', 'Scope', -1, 0, 1),
	('advanced_scope', 'Advanced Scope', -1, 0, 1),
	('extended_magazine', 'Extended Magazine', -1, 0, 1),
	('very_extended_magazine', 'Very Extended Magazine', -1, 0, 1),
	('yusuf', 'Luxury Skin', -1, 0, 1),
	('lowrider', 'Lowrider Skin', -1, 0, 1)
;
