Locales ['en'] = {

	-- Non Weapon Attachments
	['dive_suit_on'] = 'Założyłeś zestaw do nurkowania. Poziom tlenu: ~g~100~w~',
	['oxygen_notify'] = 'Ilość ~b~tlenu~y~ w baku: %s%s~w~',
	-- Weapon Clip
	['clip_use'] = 'Użyłeś magazynka',
	['clip_no_weapon'] = 'Nie masz broni w ręce',
	['clip_not_suitable'] = 'Ta amunicja nie pasuje do tej broni',
        ['redbull_consumed'] = "Wypiles Redbulla",
        ['redbull_end'] = "Efekt rebulla sie skonczyl",
}
