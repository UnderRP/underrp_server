-- Start of SERVER/MAIN.LUA

ESX = nil

local Accessories = {
	"silent",
	"flashlight",
	"grip",
	"extended_magazine",
	"very_extended_magazine",
	"scope",
	"advanced_scope",
	"yusuf",
	"lowrider",
}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

-- Start of Extra Items

ESX.RegisterUsableItem('oxygen_mask', function(source)
	TriggerClientEvent('esx_extraitems:oxygen_mask', source)
	local xPlayer  = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('oxygen_mask', 1)
end)


ESX.RegisterUsableItem('clip', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	TriggerClientEvent('esx_extraitems:clipcli', source)
	xPlayer.removeInventoryItem('clip', 1)
end)

-- End of Extra Items

ESX.RegisterUsableItem('redbull', function(source)
	TriggerClientEvent('esx_extraitems:redbull', source)
	local xPlayer = ESX.GetPlayerFromId(source)
	
	xPlayer.removeInventoryItem('redbull', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 50000)
end)

-- End of Weapon Attachments
-- End of SERVER/MAIN.LUA
