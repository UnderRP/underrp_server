INSERT INTO `addon_account` (name, label, shared) VALUES 
	('society_wydzial','Wydzial Sledczy',1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
	('society_wydzial','Wydzial Sledczy',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES 
	('society_wydzial', 'Wydzial Sledczy', 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('wydzial', 'Wydzial Sledczy', 1);

--
-- Déchargement des données de la table `jobs_grades`
--

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
('wydzial', 0, 'poczatkujacy', 'Początkujący', 1500, '{}', '{}'),
('wydzial', 1, 'bardziejdoswiadczony', 'Bardziej doświadczony', 1800, '{}', '{}'),
('wydzial', 2, 'doswiadczony', 'Doświadczony', 2100, '{}', '{}'),
('wydzial', 3, 'szef', 'Szef', 2700, '{}', '{}');