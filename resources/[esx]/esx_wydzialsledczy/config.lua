Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 6.0, y = 6.0, z = 6.0 }
Config.MarkerColor                = { r = 129, g = 207, b = 224 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.green 				  = 56108
Config.grey 				  = 8421504
Config.red 			          = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.purple 				  = 11750815
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
     { name = 'WEAPON_PISTOL',     price = 15000 },
  --    { name = 'WEAPON_MICROSMG',       price = 125000 },
   --   { name = 'WEAPON_ASSAULTRIFLE',     price = 325000 },
   --   { name = 'WEAPON_PUMPSHOTGUN',      price = 375000 },
   --   { name = 'WEAPON_STUNGUN',          price = 10000 },
   --   { name = 'WEAPON_KNIFE',       price = 500 },
   --   { name = 'WEAPON_DAGGER',     price = 500 },
    --  { name = 'WEAPON_SWITCHBLADE',      price = 500 },
	  { name = 'WEAPON_HEAVYPISTOL',         price = 35000 },
	--  { name = 'WEAPON_POOLCUE',          price = 500 },
	--  { name = 'WEAPON_GUSENBERG',        price = 225000 },
	--  { name = 'WEAPON_SMG_MK2',       price = 500000 },
    --  { name = 'WEAPON_PISTOL50',     price = 30000 },
     -- { name = 'WEAPON_BULLPUPSHOTGUN',       price = 1125000 },
     -- { name = 'WEAPON_COMPACTRIFLE',     price = 1500000 },
     -- { name = 'WEAPON_FIREEXTINGUISHER', price = 1200 },
     -- { name = 'WEAPON_FLAREGUN',         price = 6000 },
     -- { name = 'GADGET_PARACHUTE',        price = 3000 },
	 -- { name = 'WEAPON_BAT'		,        price = 3000 },
     -- { name = 'WEAPON_STICKYBOMB',       price = 200000 },
   --   { name = 'WEAPON_SNIPERRIFLE',      price = 2200000 },
     -- { name = 'WEAPON_FIREWORK',         price = 30000 },
     -- { name = 'WEAPON_GRENADE',          price = 180000 },
     -- { name = 'WEAPON_BZGAS',            price = 120000 },
    --  { name = 'WEAPON_SMOKEGRENADE',     price = 100000 },
      --{ name = 'WEAPON_APPISTOL',         price = 70000 },
      --{ name = 'WEAPON_CARBINERIFLE',     price = 1100000 },
   --   { name = 'WEAPON_HEAVYSNIPER',      price = 2000000 },
    --  { name = 'WEAPON_MINIGUN',          price = 700000 },
     -- { name = 'WEAPON_RAILGUN',          price = 2500000 },
	  
    },

	  AuthorizedVehicles = {
		  { name = 'pcs18',  label = 'Porsche Cayenne' },
      { name = 's60pol',  label = 'Dybsta 6x6' },
	--	  { name = 'btype',      label = 'Mafijny' },
	--	  { name = 'guardian',   label = 'Guardian' },
	--	  { name = 'burrito3',   label = 'Furgonetka' },
	  },

    Cloakrooms = {
      { x = 456.78, y = -993.38, z = 30.69 },
    },

    Armories = {
      { x = 451.36, y = -979.41, z = 30.69 },
    },

    Vehicles = {
      {
        Spawner    = { x = 437.41, y = -1013.92, z = 28.67 },
        SpawnPoint = { x = -1366.51, y = -659.09, z = 26.9 },
        Heading    = 185.0,
      },
    },
	
	Helicopters = {
      {
        Spawner    = { x = 535 , y = 42 , z = 300 },
        SpawnPoint = { x = 424 , y = 346 , z = 432 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 430.23, y = -1014.0, z = 28.84 },
      { x = 143.69, y = -1284.94, z = 28.4 },
    },

    BossActions = {
      { x = 457.97, y = -1008.13, z = 28.29 },
      { x = 95.12, y = -1293.91, z = 28.5 }
    },

  },

}
