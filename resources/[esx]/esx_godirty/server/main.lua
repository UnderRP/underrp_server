ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function sendToDiscord (name,message,color)
  -- Modify here your discordWebHook username = name, content = message,embeds = embeds

local embeds = {
    {
        ["title"]=message,
        ["type"]="rich",
        ["color"] =color,
        ["footer"]=  {
        ["text"]= "Pranie pieniędzy",
       },
    }
}

  if message == nil or message == '' then return FALSE end
  PerformHttpRequest(Config.webhook, function(err, text, headers) end, 'POST', json.encode({ username = name,embeds = embeds}), { ['Content-Type'] = 'application/json' })
end

RegisterServerEvent('esx_godirtyjob:pay')
AddEventHandler('esx_godirtyjob:pay', function(amount)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local blackMoney = xPlayer.getAccount('black_money').money
	local clearAmount = Config.KasaZaDostawe
	local clearAmount2 = Config.KasaZaDostaweMafia
	
	if xPlayer.job.name == 'grove' or xPlayer.job.name == 'dismay' or xPlayer.job.name == 'mafia' or xPlayer.job.name == 'drivers' or xPlayer.job.name == 'taxi' or xPlayer.job.name == 'pip' or xPlayer.job.name == 'bloody' then
		if clearAmount2 > blackMoney then
			xPlayer.addMoney(blackMoney) -- Add Clean Money
			xPlayer.removeAccountMoney('black_money', blackMoney) -- Removes Dirty Money
			sendToDiscord('Pralnia pieniędzy', xPlayer.name ..' wyczyścił $'.. blackMoney ..' brudnych pieniędzy.',Config.purple)
		else
			xPlayer.addMoney(clearAmount2) -- Add Clean Money
			xPlayer.removeAccountMoney('black_money', clearAmount2) -- Removes Dirty Money
			sendToDiscord('Pralnia pieniędzy', xPlayer.name ..' wyczyścił $'.. clearAmount2 ..' brudnych pieniędzy.',Config.purple)
		end
	else
		if clearAmount > blackMoney then
			xPlayer.addMoney(blackMoney) -- Add Clean Money
			xPlayer.removeAccountMoney('black_money', blackMoney) -- Removes Dirty Money
			sendToDiscord('Pralnia pieniędzy', xPlayer.name ..' wyczyścił $'.. blackMoney ..' brudnych pieniędzy.',Config.purple)
		else
			xPlayer.addMoney(tonumber(amount)) -- Add Clean Money
			xPlayer.removeAccountMoney('black_money', amount) -- Removes Dirty Money
			sendToDiscord('Pralnia pieniędzy', xPlayer.name ..' wyczyścił $'.. amount ..' brudnych pieniędzy.',Config.purple)
		end
	end
end)
