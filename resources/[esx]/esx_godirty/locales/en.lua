Locales['en'] = {
	['cloakroom']				= 'Szatnia',
	['job_wear']				= 'Ubranie Robocze',
	['citizen_wear']			= 'Ubranie Cywilne',
	['vehiclespawner']			= 'Wybierz Pojazd', 
	['already_have_truck']		= 'Nie odstawiłeś jeszcze poprzedniego pojazdu!', 
	['delivery']				= 'wciśnij ~INPUT_PICKUP~ aby przeprać pieniądze.',
	['not_your_truck']			= 'To nie jest pojazd który wypożyczyłeś !',
	['not_your_truck2']			= 'Musisz być w przydzielonym Ci wcześniej pojeździe !',
	['need_it']					= 'Taaa, ale potrzebujemy tego !',
	['ok_work']					= 'Super, pracuj dalej !',
	['scared_me']				= 'ok, prawie mnie przestraszyłeś !',
	['resume_delivery']			= 'spoko, kontynuuj pracę !',
	['no_delivery']				= 'Nie dostarczyłeś żadnego ładunku, nie wypierzesz kaski',
	['pay_repair']				= 'Zniszczyłeś pojazd, zapłacisz za to !',
	['repair_minus']			= 'Naprawa pojazdu : -',
	['shipments_plus']			= 'Wynagrodzenie : +',
	['truck_state']				= 'nie masz kasy brachu, weź się do roboty !',
	['no_delivery_no_truck']	= 'nic nie zrobiłeś, straciłeś auto, to jakiś żart ?',
	['truck_price']				= 'Cena wypożyczenia pojazdu : -',
	['meet_ls']					= 'Dostarcz ładunek do Los Santos',
	['meet_bc']					= 'Dostarcz ładunek do Blaine County',
	['meet_del']				= 'Jedź do punktu dostawy',
	['return_depot']			= 'Wróć do kasyna',
	['blip_job']				= 'Post Office',
	['blip_delivery']			= 'Post Office : Delivery',
	['blip_goal']				= 'Post Office : Delivery point',
	['blip_depot']				= 'Post Office : depot',
	['cancel_mission']			= 'Wciśnij ~INPUT_PICKUP~ aby ~r~Zwrócić~w~ pojazd',
	['not_delivery']			= '~g~Dziękujemy za uczynną pracę!',
	['Money_Laundering']		= 'Pralnia pieniędzy'
}

--------------------------------------------------------------------------
------------------------------BY  K R I Z F R O S T-----------------------
--------------------------------------------------------------------------
----- MONEY LAUNDERING SCRIPT RELEASED CREDITS TO ORIGINAL CREATOR--------
----- OF ESX_POSTALJOB RE WORKED AND MODIFIED BY KRIZFROST ---------------
--------------------------------------------------------------------------