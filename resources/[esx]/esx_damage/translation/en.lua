Locales['en'] = {
  -- regulars
  	['press_e'] = 'Wciśnij ~INPUT_CONTEXT~ aby sprawdzić, jak ta osoba ~r~straciła przytomność~s~',
	['hardmeele'] = 'Prawdopodobnie trafiony przez coś twardego w głowie',
	['bullet'] = 'Prawdopodobnie postrzelony, dziury po pociskach w ciele',
	['knifes'] = 'Prawdopodobnie zaatakowany czymś ostrym',
	['bitten'] = 'Prawdopodobnie pogryziony przez zwierzę',
	['brokenlegs'] = 'Prawdopodobnie upadł, złamał obie nogi',
	['explosive'] = 'Prawdopodobnie umarł przez coś, co eksploduje',
	['gas'] = 'Prawdopodobnie stracił przytomność przez uszkodzenie płuc - gaz',
	['fireextinguisher'] = 'Prawdopodobnie stracił przytomność przez uszkodzenie płuc - gaśnica',
	['fire'] = 'Prawdopodobnie zginął przy ogniu',
	['caraccident'] = 'Prawdopodobnie stracił przytomność w wypadku samochodowym',
	['drown'] = 'Prawdopodobnie utonął',
	['unknown'] = 'Przyczyna nieznana',
}
