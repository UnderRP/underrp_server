Config = {}
Config.DrawDistance = 20.0
Config.HideRadar = true -- Hide HUD?
Config.AnimTime = 60 -- Animation for the hacking in seconds. 60 = 1 minute / 60 seconds!

Config.Locale = 'en'
Config.pNotify = true -- Only enable this if you have pNotify (https://github.com/Nick78111/pNotify)
Config.Hacking = false -- Only enable if you have mhacking (https://github.com/GHMatti/FiveM-Scripts/tree/master/mhacking)

-- Connect to the cameras
-- Place: In the polices armory room
Config.Zones = {
	Cameras = {
		Pos   = {x = -1912.43, y = -571.12, z = 18.2},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	},
	CamerasB = {
		Pos   = {x = 247.81, y = -298.51, z = -2.05},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	}
}

-- Keep these the same
-- Place: Behind the polices desk in the policestation
-- Screenshot: https://i.imgur.com/f5WNrRj.jpg
Config.HackingPolice = {
	HackingPolice = {
		Pos   = {x = 440.17, y = -975.74, z = 29.69},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	}
}

Config.UnHackPolice = {
	UnHackPolice = {
		Pos   = {x = 440.17, y = -975.74, z = 29.69},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	}
}

-- Keep these the same
-- Place: Down at the bank vault
-- Screenshot: https://i.imgur.com/nvcFUhu.jpg
Config.HackingBank = {
	HackingBank = {
		Pos   = {x = 264.87, y = 219.93, z = 100.68},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	}
}

Config.UnHackBank = {
	UnHackBank = {
		Pos   = {x = 264.87, y = 219.93, z = 100.68},
		Size  = {x = 1.7, y = 1.7, z = 0.5},
		Color = {r = 26, g = 55, b = 186},
		Type = 27,
	}
}

-- Cameras. You could add more cameras for other banks, apartments, houses, buildings etc. (Remember the "," after each row, but not on the last row)
Config.Locations = {
	{
	bankCamLabel = {label = _U('dom')},
		bankCameras = {
			{label = _U('z1'), x = -1907.38, y = -579.81, z = 21.1, r = {x = -35.0, y = 0.0, z = 60.00}, canRotate = true},
			{label = _U('z2'), x = -1907.27, y = -583.13, z = 13.39, r = {x = -35.0, y = 0.0, z = 330.00}, canRotate = true},
			{label = _U('z3'), x = -1899.03, y = -566.43, z = 13.28, r = {x = -35.0, y = 0.0, z = 0.00}, canRotate = true},
			{label = _U('z4'), x = -1906.41, y = -557.27, z = 14.3, r = {x = -35.0, y = 0.0, z = 260.00}, canRotate = true}
		},

	policeCamLabel = {label = _U('okolicedomu')},
		policeCameras = {
			{label = _U('s1'), x = -1901.27, y = -572.99, z = 21.1, r = {x = -35.0, y = 0.0, z = 90.00}, canRotate = true}
		},
	}
}
