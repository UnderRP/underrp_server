Locales['pl'] = {

  ['marker_hint'] = 'Nacisnij ~INPUT_CONTEXT~ aby przegladac ~g~MONITORING~s~',


  ['securitycams_menu'] = 'Monitoring',
  ['bank_menu_selection'] = 'Pokoj(1)',
  ['police_menu_selection'] = 'Zewnątrz(4)',


  ['pacific_standard_bank'] = 'Pokoj',
  ['police_station'] = 'Okolice domu',


  ['s1'] = '',


  ['z1'] = 'Balkon',
  ['z2'] = 'Drzwi frontowe',
  ['z3'] = 'Parking #1',
  ['z4'] = 'Parking #2',


  ['next'] = 'NASTEPNA KAMERA',
  ['previous'] = 'POPRZEDNIA KAMERA',
  ['close'] = 'ZAMKNIJ MONITORING',
}
