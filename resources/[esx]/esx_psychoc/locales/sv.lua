Locales['sv'] = {

  ['marker_hint'] = 'Nacisnij ~INPUT_CONTEXT~ aby przegladac ~g~MONITORING~s~',


  ['securitycams_menu'] = 'Monitoring',
  ['bank_menu_selection'] = 'Okolice domu(4)',
  ['police_menu_selection'] = 'Dom(1)',


  ['dom'] = 'Okolice domu',
  ['okolicedomu'] = 'Dom',


  ['s1'] = 'Pokoj',


  ['z1'] = 'Balkon',
  ['z2'] = 'Drzwi frontowe',
  ['z3'] = 'Parking #1',
  ['z4'] = 'Parking #2',


  ['next'] = 'NASTEPNA KAMERA',
  ['previous'] = 'POPRZEDNIA KAMERA',
  ['close'] = 'ZAMKNIJ MONITORING',
}
