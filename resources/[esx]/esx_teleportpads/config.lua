Config              = {}

Config.DrawDistance = 0.0

Config.Marker = {
	Type = 27,
	x = 1.5, y = 1.5, z = 2.0,
	r = 255, g = 255, b = 255
}

Config.Pads = {

	--MartinHouseIn = {
	--	Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~domu~s~.',
	--	Marker = { x = -1537.27, y = 130.2, z = 56.99 },
	--	TeleportPoint = { x = 1397.11, y = 1141.11, z = 113.33, h = 272.6 }
	--},
    --
	--MartinHouseOut = {
	--	Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~domu~s~.',
	--	Marker = { x = 1397.11, y = 1141.72, z = 113.33 },
	--	TeleportPoint = { x = -1537.27, y = 130.2, z = 56.4, h = 83.2 }
	--},
    --
	--WejscieDrivers = {
	--	Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~domu~s~.',
	--	Marker = { x = -72.58, y = 81.31, z = 71.7 },
	--	TeleportPoint = { x = 998.89, y = -3158.17, z = -38.91, h = 244.3 }
	--},
    --
	--WyjscieDrivers = {
	--	Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~domu~s~.',
	--	Marker = { x = 997.5, y = -3158.02, z = -38.91 },
	--	TeleportPoint = { x = -72.58, y = 81.31, z = 71.7, h = 236.7 }
	--},
	WejscieBahama = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~domu~s~.',
		Marker = { x = -1389.0013, y = -586.41, z = 29.819 },
		TeleportPoint = { x = -1388.0013, y = -588.41, z = 30.91, h = 244.3 }
	},

	WyjscieBahama = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~domu~s~.',
		Marker = { x = -1387.0013, y = -588.41, z = 29.91},
		TeleportPoint = { x = -1389.0013, y = -586.41, z = 30.819, h = 244.3}
	},


	WejscieMafia = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~pokoju~s~.',
		Marker = { x = 1231.3, y = -3163.83, z = 9.06 },
		TeleportPoint = { x = 1227.38, y = -3126.45, z = 6.11, h = 3.7 }
	},

	WyjscieMafia = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~pokoju~s~.',
		Marker = { x = 1227.38, y = -3126.45, z = 6.11 },
		TeleportPoint = { x = 1231.3, y = -3163.83, z = 9.06, h = 83.2 }
	},
	InzeIn = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~domu~s~.',
		Marker = { x = 1535.09, y = 2232.02, z = 77.7 },
		TeleportPoint = { x = 248.62, y = -292.82, z = -1.9, h = 240.6 }
	},

	InzeOut = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~domu~s~.',
		Marker = { x = 248.62, y = -292.82, z = -1.9 },
		TeleportPoint = { x = 1535.09, y = 2232.02, z = 77.7, h = 93.2 }
	},


}