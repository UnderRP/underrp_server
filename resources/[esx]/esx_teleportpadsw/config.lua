Config              = {}

Config.DrawDistance = 100.0

Config.Marker = {
	Type = 27,
	x = 1.0, y = 1.0, z = 2.0,
	r = 203, g = 203, b = 0
}

Config.Pads = {

	WejscieDrivers = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~domu~s~.',
		Marker = { x = -72.58, y = 81.31, z = 70.7 },
		TeleportPoint = { x = 998.89, y = -3158.17, z = -38.91, h = 244.3 }
	},

	WyjscieDrivers = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~domu~s~.',
		Marker = { x = 997.5, y = -3158.02, z = -39.86 },
		TeleportPoint = { x = -72.58, y = 81.31, z = 71.7, h = 236.7 }
	},
	
	WejscieSala = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść na ~y~sale~s~.',
		Marker = { x = 470.6, y = -984.88, z = 29.69 },
		TeleportPoint = { x = 442.12, y = -997.06, z = 4.8, h = 180.4 }
	},

	WyjscieSala = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~sali~s~.',
		Marker = { x = 442.12, y = -997.06, z = 3.8 },
		TeleportPoint = { x = 470.6, y = -984.88, z = 30.69, h = 94.6 }
	},

	WejscieKonferencja = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść na ~y~sale~s~.',
		Marker = { x = 451.97, y = -988.32, z = 25.67 },
		TeleportPoint = { x = 442.06, y = -986.37, z = 4.8, h = 346.4 }
	},

	WyjscieKonferencja = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~sali~s~.',
		Marker = { x = 442.06, y = -986.37, z = 3.8 },
		TeleportPoint = { x = 451.97, y = -988.32, z = 26.67, h = 2.2 }
	},
	Wejsciehol = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść na ~y~sale~s~.',
		Marker = { x = 224.49, y = -442.6, z = 44.25 },
		TeleportPoint = { x = 224.88, y = -419.58, z = -118.2, h = 248.8 }
	},

	Wyjsciehol = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~sali~s~.',
		Marker = { x = 224.88, y = -419.58, z = -119.2 },
		TeleportPoint = { x = 224.49, y = -442.6, z = 45.25, h = 169.5 }
	},
	Wejsciesad = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść na ~y~sale~s~.',
		Marker = { x = 236.06, y = -413.81, z = -119.16 },
		TeleportPoint = { x = 238.85, y = -333.9, z = -118.77, h = 337.4 }
	},

	Wyjsciesad = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~sali~s~.',
		Marker = { x = 238.85, y = -333.9, z = -119.77 },
		TeleportPoint = { x = 236.06, y = -413.81, z = -118.16, h = 155.2 }
	},
	WejscieMagazyn = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~magazynu~s~.',
		Marker = { x = 1181.32, y = -3113.85, z = 5.1753 },
		TeleportPoint = { x= 992.8129, y = -3097.4555, z = -40.0, h = 260.0 }
	},
	WyjscieMagazyn = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~magazynu~s~.',
		Marker = { x= 992.8129, y = -3097.4555, z = -41.0 },
		TeleportPoint = { x = 1181.9100, y = -3113.7482, z = 6.1753 , h = 63.4 }
	},
	Wejscieoskar = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wejść do ~y~pokoju~s~.',
		Marker = { x = 246.08, y = -337.13, z = -119.8 },
		TeleportPoint = { x = 248.95, y = -337.56, z = -118.8, h = 260.0 }
	},

	Wyjscieoskar = {
		Text = 'Naciśnij ~INPUT_CONTEXT~ aby wyjść z ~y~pokoju~s~.',
		Marker = { x = 248.95, y = -337.56, z = -119.8 },
		TeleportPoint = { x = 245.99, y = -337.04, z = -118.8 , h = 63.4 }
	},



}