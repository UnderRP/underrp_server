Locales['en'] = {
  ['skin_menu'] = 'Wygląd',
  ['use_rotate_view'] = 'użyj ~INPUT_VEH_FLY_ROLL_LEFT_ONLY~ i ~INPUT_VEH_FLY_ROLL_RIGHT_ONLY~ aby obrócić kamerę.',
  ['skin'] = 'Zmień wygląd',
  ['saveskin'] = 'Zapisz wygląd',
  ['confirm_escape'] = 'Czy chcesz anulować? Wszystkie zmiany zostaną utracone.',
  ['no'] = 'nie',
  ['yes'] = 'tak',
}
