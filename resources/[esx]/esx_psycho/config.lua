Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false
Config.EnableESXIdentity          = true -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 5000 },
      { name = 'WEAPON_STUNGUN',          price = 15000 },
    },

	  AuthorizedVehicles = {
		  { name = 'm2',  label = 'BMW M2' },
	  },

    Cloakrooms = {
      { x = -1905.58, y = -571.21, z = 18.1 },
    },

    Armories = {
      { x = -1909.34, y = -571.45, z = 18.1 },
    },

    Vehicles = {
      {
        Spawner    = { x = -1902.44, y = -562.85, z = 10.82 },
        SpawnPoint = { x = -1898.78, y = -559.18, z = 11.75 },
        Heading    = 310.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -1904.71, y = -558.11, z = 10.8 },
    },

    BossActions = {
      { x = -1911.93, y = -569.77, z = 18.1 }
    },

  },

}