Locales ['pl'] = {

	['valid_this_purchase'] = 'Czy chcesz kupić?',
	['yes'] = 'Tak',
	['no'] = 'Nie',
	['name_outfit'] = 'Chcesz nazwać swój strój?',
	['not_enough_money'] = 'Nie masz przy sobie pieniędzy',
	['press_menu'] = 'Naciśnij ~INPUT_CONTEXT~ aby sie przebrać',
	['clothes'] = 'Ubrania',
	['you_paid'] = 'Zapłaciłeś $',
	['save_in_dressing'] = 'Do you want to give a name to your outfit ?',
	['saved_outfit'] = 'Zapisano strój'
}
