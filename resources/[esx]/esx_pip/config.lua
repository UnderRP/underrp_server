Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 2.0, y = 2.0, z = 1.0 }
Config.MarkerColor                = { r = 255, g = 255, b = 0 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale = 'en'

Config.green 				  = 56108
Config.grey 				  = 8421504
Config.red 			          = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.purple 				  = 11750815
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_PISTOL',     price = 15000 },
  --    { name = 'WEAPON_MICROSMG',       price = 125000 },
   --   { name = 'WEAPON_ASSAULTRIFLE',     price = 325000 },
   --   { name = 'WEAPON_PUMPSHOTGUN',      price = 375000 },
   --   { name = 'WEAPON_STUNGUN',          price = 10000 },
      { name = 'WEAPON_KNIFE',       price = 500 },
      { name = 'WEAPON_DAGGER',     price = 500 },
    --  { name = 'WEAPON_SWITCHBLADE',      price = 500 },
	  { name = 'WEAPON_HEAVYPISTOL',         price = 35000 },
	  { name = 'WEAPON_POOLCUE',          price = 500 },
	--  { name = 'WEAPON_GUSENBERG',        price = 225000 },
	--  { name = 'WEAPON_SMG_MK2',       price = 500000 },
    --  { name = 'WEAPON_PISTOL50',     price = 30000 },
     -- { name = 'WEAPON_BULLPUPSHOTGUN',       price = 1125000 },
     -- { name = 'WEAPON_COMPACTRIFLE',     price = 1500000 },
     -- { name = 'WEAPON_FIREEXTINGUISHER', price = 1200 },
     -- { name = 'WEAPON_FLAREGUN',         price = 6000 },
     -- { name = 'GADGET_PARACHUTE',        price = 3000 },
	  { name = 'WEAPON_BAT'		,        price = 3000 },
     -- { name = 'WEAPON_STICKYBOMB',       price = 200000 },
   --   { name = 'WEAPON_SNIPERRIFLE',      price = 2200000 },
     -- { name = 'WEAPON_FIREWORK',         price = 30000 },
     -- { name = 'WEAPON_GRENADE',          price = 180000 },
     -- { name = 'WEAPON_BZGAS',            price = 120000 },
    --  { name = 'WEAPON_SMOKEGRENADE',     price = 100000 },
      -- { name = 'WEAPON_APPISTOL',         price = 70000 },
      -- { name = 'WEAPON_CARBINERIFLE',     price = 1100000 },
   --   { name = 'WEAPON_HEAVYSNIPER',      price = 2000000 },
    --  { name = 'WEAPON_MINIGUN',          price = 700000 },
     -- { name = 'WEAPON_RAILGUN',          price = 2500000 },
	  
    },

	  AuthorizedVehicles = {
		  { name = 'a8audi',  label = 'Audi A8' },
	--	  { name = 'btype',      label = 'Mafijny' },
	--	  { name = 'guardian',   label = 'Guardian' },
	--	  { name = 'burrito3',   label = 'Furgonetka' },
	  },

    Cloakrooms = {
      { x = 1392.14, y = 1144.94, z = 113.4 },
    },

    Armories = {
      { x = 1399.23, y = 1161.41, z = 113.34 },
    },

    Vehicles = {
      {
        Spawner    = { x = -1533.92, y = 81.07, z = 55.78 },
        SpawnPoint = { x = -1525.05, y = 95.4, z = 56.64 },
        Heading    = 185.0,
      }
    },
	
	Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = -173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = -177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -1524.26, y = 80.59, z = 55.7 },
	  { x = -1528.31, y = 79.99, z = 55.71 },
    },

    BossActions = {
      { x = 1401.58, y = 1132.25, z = 113.34 }
    },

  },

}
