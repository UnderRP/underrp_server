Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false
Config.EnableESXIdentity          = true -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale = 'en'

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 5000 },
      { name = 'WEAPON_STUNGUN',          price = 15000 },
    },

	  AuthorizedVehicles = {
		  { name = 'rrphantom',  label = 'Rolls Royce Phantom' },
	  },

    Cloakrooms = {
      { x = -1905.58, y = -571.21, z = 18.1 },
    },

    Armories = {
      { x = -1909.34, y = -571.45, z = 18.1 },
    },

    Vehicles = {
      {
        Spawner    = { x = -111.37, y = -608.22, z = 35.3 },
        SpawnPoint = { x = -105.05, y = -610.45, z = 35.1 },
        Heading    = 160.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = -1111.37, y = -608.22, z = 35.3 },
        SpawnPoint = { x = -1105.05, y = -610.45, z = 35.1 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -101.03, y = -598.45, z = 35.1 },
    },

    BossActions = {
      { x = -128.02, y = -634.04, z = 167.82 }
    },

  },

}