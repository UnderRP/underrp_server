Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 2.0, y = 2.0, z = 1.0 }
Config.MarkerColor                = { r = 255, g = 255, b = 0 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.green 				  = 56108
Config.grey 				  = 8421504
Config.red 			          = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.purple 				  = 11750815
Config.webhook                = "https://discordapp.com/api/webhooks/551729807487139841/51IaUYoZn0P0sSUXa9RnGB5OGKQ8NwvVg-242x3nee3OmbFSPKU5JGfFDJlUGb-Waeip"

Config.MafiaStations = {

  Mafia = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 9000 },
      { name = 'WEAPON_COMBATPISTOL',     price = 300000 },
      { name = 'WEAPON_ASSAULTSMG',       price = 11250000 },
      { name = 'WEAPON_ASSAULTRIFLE',     price = 15000000 },
      { name = 'WEAPON_PUMPSHOTGUN',      price = 60000000 },
      { name = 'WEAPON_STUNGUN',          price = 500000 },
      { name = 'WEAPON_FLASHLIGHT',       price = 800 },
      { name = 'WEAPON_FIREEXTINGUISHER', price = 12000 },
      { name = 'WEAPON_FLAREGUN',         price = 600000 },
      { name = 'GADGET_PARACHUTE',        price = 300000 },
     -- { name = 'WEAPON_STICKYBOMB',       price = 200000 },
      { name = 'WEAPON_SNIPERRIFLE',      price = 20000000 },
      -- { name = 'WEAPON_FIREWORK',         price = 300000 },
     -- { name = 'WEAPON_GRENADE',          price = 180000 },
     -- { name = 'WEAPON_BZGAS',            price = 120000 },
     -- { name = 'WEAPON_SMOKEGRENADE',     price = 100000 },
     -- { name = 'WEAPON_APPISTOL',         price = 70000 },
      { name = 'WEAPON_CARBINERIFLE',     price = 11000000 },
      { name = 'WEAPON_HEAVYSNIPER',      price = 800000000 },
     -- { name = 'WEAPON_MINIGUN',          price = 700000 },
     -- { name = 'WEAPON_RAILGUN',          price = 2500000 },
    },

	  AuthorizedVehicles = {
		  { name = 'schafter3',  label = 'Véhicule Civil' },
		  { name = 'sandking',   label = '4X4' },
		  { name = 'mule3',      label = 'Camion de Transport' },
		  { name = 'guardian',   label = 'Grand 4x4' },
		  { name = 'burrito3',   label = 'Fourgonnette' },
		  { name = '2016rs7',  label = 'AudiRS7' },
		  { name = 'mesa',       label = 'Tout-Terrain' },
	  },

    Cloakrooms = {
      { x = 9.283, y = 528.914, z = 169.635 },
    },

    Armories = {
      { x = -799.82226562, y = 177.35665893554, z = 72.834732055664 },
    },

    Vehicles = {
      {
        Spawner    = { x = -820.86254882812, y = 184.03034973144, z = 72.06411743164 },
        SpawnPoint = { x = -826.80834960938, y = 177.85037231446, z = 71.162506103516 },
        Heading    = 90.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -811.4506225586, y = 187.39477539062, z = 72.47787475586 },
      { x = 21.35, y = 543.3, z = 175.027 },
    },

    BossActions = {
      { x = -803.80419921875, y = 172.86164855958, z = 72.84465789795 }
    },

  },

}