ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function sendToDiscord (name,message,color)
  -- Modify here your discordWebHook username = name, content = message,embeds = embeds

local embeds = {
    {
        ["title"]=message,
        ["type"]="rich",
        ["color"] =color,
        ["footer"]=  {
        ["text"]= "Przy użyciu pluginu esx_drivers",
       },
    }
}

  if message == nil or message == '' then return FALSE end
  PerformHttpRequest(Config.webhook, function(err, text, headers) end, 'POST', json.encode({ username = name,embeds = embeds}), { ['Content-Type'] = 'application/json' })
end

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'drivers', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'drivers', _U('alert_drivers'), true, true)
TriggerEvent('esx_society:registerSociety', 'drivers', 'Drivers', 'society_drivers', 'society_drivers', 'society_drivers', {type = 'public'})

RegisterServerEvent('esx_driversjob:giveWeapon')
AddEventHandler('esx_driversjob:giveWeapon', function(weapon, ammo)
  local xPlayer = ESX.GetPlayerFromId(source)
  xPlayer.addWeapon(weapon, ammo)
end)

RegisterServerEvent('esx_driversjob:confiscatePlayerItem')
AddEventHandler('esx_driversjob:confiscatePlayerItem', function(target, itemType, itemName, amount)

  local sourceXPlayer = ESX.GetPlayerFromId(source)
  local targetXPlayer = ESX.GetPlayerFromId(target)
  local wartosc = amount

  if itemType == 'item_standard' then

    local label = sourceXPlayer.getInventoryItem(itemName).label

    targetXPlayer.removeInventoryItem(itemName, amount)
    sourceXPlayer.addInventoryItem(itemName, amount)

    TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_have_confinv') .. amount .. ' ' .. label .. _U('from') .. targetXPlayer.name)
    TriggerClientEvent('esx:showNotification', targetXPlayer.source, '~b~' .. targetXPlayer.name .. _U('confinv') .. amount .. ' ' .. label )
	sendToDiscord('Drivers Inventory', sourceXPlayer.name ..' skonfiskował '.. amount .. ' '.. label.. ' graczowi '.. targetXPlayer.name,Config.purple)
  end

  if itemType == 'item_account' then

    targetXPlayer.removeAccountMoney(itemName, amount)
    sourceXPlayer.addAccountMoney(itemName, amount)

    TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_have_confdm') .. amount .. _U('from') .. targetXPlayer.name)
    TriggerClientEvent('esx:showNotification', targetXPlayer.source, '~b~' .. targetXPlayer.name .. _U('confdm') .. amount)
	sendToDiscord('Drivers Inventory', sourceXPlayer.name ..' skonfiskował '.. amount .. ' '.. itemName.. ' graczowi '.. targetXPlayer.name,Config.purple)

  end

  if itemType == 'item_weapon' then

    targetXPlayer.removeWeapon(itemName)
    sourceXPlayer.addWeapon(itemName, amount)

    TriggerClientEvent('esx:showNotification', sourceXPlayer.source, _U('you_have_confweapon') .. ESX.GetWeaponLabel(itemName) .. _U('from') .. targetXPlayer.name)
    TriggerClientEvent('esx:showNotification', targetXPlayer.source, '~b~' .. targetXPlayer.name .. _U('confweapon') .. ESX.GetWeaponLabel(itemName))
	sendToDiscord('Drivers Inventory', sourceXPlayer.name ..' skonfiskował '.. ESX.GetWeaponLabel(itemName).. ' graczowi '.. targetXPlayer.name,Config.purple)

  end

end)

RegisterServerEvent('esx_driversjob:handcuff')
AddEventHandler('esx_driversjob:handcuff', function(target)
  TriggerClientEvent('esx_driversjob:handcuff', target, source)
end)

RegisterServerEvent('esx_drivers:handcuffon')
AddEventHandler('esx_drivers:handcuffon', function(cop)
	local xPlayer = ESX.GetPlayerFromId(cop)
	local xTarget = ESX.GetPlayerFromId(source)
  TriggerClientEvent('esx:showNotification', cop,  _U('handcuff_on'))
  TriggerClientEvent('esx:showNotification', source,  _U('handcuff_onme'))
  sendToDiscord('Drivers handcuffs', xPlayer.name ..' skuł kajdankami '.. xTarget.name,Config.purple)
end)

RegisterServerEvent('esx_drivers:handcuffoff')
AddEventHandler('esx_drivers:handcuffoff', function(cop)
local xPlayer = ESX.GetPlayerFromId(cop)
	local xTarget = ESX.GetPlayerFromId(source)
  TriggerClientEvent('esx:showNotification', cop,  _U('handcuff_off'))
  TriggerClientEvent('esx:showNotification', source,  _U('handcuff_offme'))
   sendToDiscord('Drivers handcuffs', xPlayer.name ..' rozkuł kajdankami '.. xTarget.name,Config.purple)
end)

RegisterServerEvent('esx_driversjob:drag')
AddEventHandler('esx_driversjob:drag', function(target)
  local _source = source
  TriggerClientEvent('esx_driversjob:drag', target, _source)
    TriggerClientEvent('esx:showNotification', target,  _U('handcuff_dragme'))
  TriggerClientEvent('esx:showNotification', source,  _U('handcuff_drag'))
end)

RegisterServerEvent('esx_driversjob:putInVehicle')
AddEventHandler('esx_driversjob:putInVehicle', function(target)
  TriggerClientEvent('esx_driversjob:putInVehicle', target)
  TriggerClientEvent('esx:showNotification', source,  _U('handcuff_putinveh'))
end)

RegisterServerEvent('esx_driversjob:OutVehicle')
AddEventHandler('esx_driversjob:OutVehicle', function(target)
    TriggerClientEvent('esx_driversjob:OutVehicle', target)
	TriggerClientEvent('esx:showNotification', source,  _U('handcuff_outofveh'))
end)

RegisterServerEvent('esx_driversjob:getStockItem')
AddEventHandler('esx_driversjob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_drivers', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
	  sendToDiscord('Drivers Inventory', xPlayer.name ..' wziął z szafki frakcyjnej '.. count .. ' '.. item.label,Config.purple)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

  end)

end)

RegisterServerEvent('esx_driversjob:putStockItems')
AddEventHandler('esx_driversjob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_drivers', function(inventory)

    local item = inventory.getItem(itemName)
	local itemCount = xPlayer.getInventoryItem(itemName).count

    if count >= 0 and itemCount >= count then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
	  sendToDiscord('Drivers Inventory', xPlayer.name ..' wsadził do szafki frakcyjnej '.. count .. ' '.. item.label,Config.purple)
	  TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end


  end)

end)

ESX.RegisterServerCallback('esx_driversjob:getOtherPlayerData', function(source, cb, target)

  if Config.EnableESXIdentity then

    local xPlayer = ESX.GetPlayerFromId(target)

    local identifier = GetPlayerIdentifiers(target)[1]

    local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", {
      ['@identifier'] = identifier
    })

    local user      = result[1]
    local firstname     = user['firstname']
    local lastname      = user['lastname']
    local sex           = user['sex']
    local dob           = user['dateofbirth']
    local height        = user['height'] .. " Inches"

    local data = {
      name        = GetPlayerName(target),
      job         = xPlayer.job,
      inventory   = xPlayer.inventory,
      accounts    = xPlayer.accounts,
      weapons     = xPlayer.loadout,
      firstname   = firstname,
      lastname    = lastname,
      sex         = sex,
      dob         = dob,
      height      = height
    }

    TriggerEvent('esx_status:getStatus', source, 'drunk', function(status)

      if status ~= nil then
        data.drunk = math.floor(status.percent)
      end

    end)

    if Config.EnableLicenses then

      TriggerEvent('esx_license:getLicenses', source, function(licenses)
        data.licenses = licenses
        cb(data)
      end)

    else
      cb(data)
    end

  else

    local xPlayer = ESX.GetPlayerFromId(target)

    local data = {
      name       = GetPlayerName(target),
      job        = xPlayer.job,
      inventory  = xPlayer.inventory,
      accounts   = xPlayer.accounts,
      weapons    = xPlayer.loadout
    }

    TriggerEvent('esx_status:getStatus', _source, 'drunk', function(status)

      if status ~= nil then
        data.drunk = status.getPercent()
      end

    end)

    TriggerEvent('esx_license:getLicenses', _source, function(licenses)
      data.licenses = licenses
    end)

    cb(data)

  end

end)

ESX.RegisterServerCallback('esx_driversjob:getFineList', function(source, cb, category)

  MySQL.Async.fetchAll(
    'SELECT * FROM fine_types_drivers WHERE category = @category',
    {
      ['@category'] = category
    },
    function(fines)
      cb(fines)
    end
  )

end)

ESX.RegisterServerCallback('esx_driversjob:getVehicleInfos', function(source, cb, plate)

  if Config.EnableESXIdentity then

    MySQL.Async.fetchAll(
      'SELECT * FROM owned_vehicles',
      {},
      function(result)

        local foundIdentifier = nil

        for i=1, #result, 1 do

          local vehicleData = json.decode(result[i].vehicle)

          if vehicleData.plate == plate then
            foundIdentifier = result[i].owner
            break
          end

        end

        if foundIdentifier ~= nil then

          MySQL.Async.fetchAll(
            'SELECT * FROM users WHERE identifier = @identifier',
            {
              ['@identifier'] = foundIdentifier
            },
            function(result)

              local ownerName = result[1].firstname .. " " .. result[1].lastname

              local infos = {
                plate = plate,
                owner = ownerName
              }

              cb(infos)

            end
          )

        else

          local infos = {
          plate = plate
          }

          cb(infos)

        end

      end
    )

  else

    MySQL.Async.fetchAll(
      'SELECT * FROM owned_vehicles',
      {},
      function(result)

        local foundIdentifier = nil

        for i=1, #result, 1 do

          local vehicleData = json.decode(result[i].vehicle)

          if vehicleData.plate == plate then
            foundIdentifier = result[i].owner
            break
          end

        end

        if foundIdentifier ~= nil then

          MySQL.Async.fetchAll(
            'SELECT * FROM users WHERE identifier = @identifier',
            {
              ['@identifier'] = foundIdentifier
            },
            function(result)

              local infos = {
                plate = plate,
                owner = result[1].name
              }

              cb(infos)

            end
          )

        else

          local infos = {
          plate = plate
          }

          cb(infos)

        end

      end
    )

  end

end)

ESX.RegisterServerCallback('esx_driversjob:getArmoryWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_drivers', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_driversjob:addArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeWeapon(weaponName)
  sendToDiscord('Drivers Inventory', xPlayer.name ..' wsadził do składu broni '.. weaponName,Config.purple)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_drivers', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_driversjob:removeArmoryWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.addWeapon(weaponName, 1000)
  sendToDiscord('Drivers Inventory', xPlayer.name ..' wyciągnął ze składu broni '.. weaponName,Config.purple)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_drivers', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)


ESX.RegisterServerCallback('esx_driversjob:buy', function(source, cb, amount)

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_drivers', function(account)

    if account.money >= amount then
      account.removeMoney(amount)
      cb(true)
    else
      cb(false)
    end

  end)

end)

ESX.RegisterServerCallback('esx_driversjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_drivers', function(inventory)
    cb(inventory.items)
  end)

end)

ESX.RegisterServerCallback('esx_driversjob:getPlayerInventory', function(source, cb)

  local xPlayer = ESX.GetPlayerFromId(source)
  local items   = xPlayer.inventory

  cb({
    items = items
  })

end)
