Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.DriversStations = {

  Drivers = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_VINTAGEPISTOL',           price = 60000 },
      { name = 'WEAPON_COMBATPISTOL',     price = 20000 },
	  { name = 'WEAPON_MICROSMG',              price = 125000 },
	  { name = 'WEAPON_PUMPSHOTGUN',    price = 500000 },
	  { name = 'WEAPON_COMBATPDW',    price = 300000 },
	  { name = 'WEAPON_STUNGUN',          price = 250 },
      { name = 'WEAPON_FLASHLIGHT',       price = 50 },
      { name = 'WEAPON_FLAREGUN',         price = 3000 },
      { name = 'WEAPON_FLARE',            price = 8000 },
      { name = 'WEAPON_SWITCHBLADE',      price = 500 },
	  { name = 'WEAPON_POOLCUE',          price = 100 },
	  
	  
    },

	  AuthorizedVehicles = {
		  { name = 'pony',  label = 'Pony' },
		  { name = 'burrito2',      label = 'Burrito' },
		  { name = 'boxville2',      label = 'boxville' },
		  { name = 'guardian',   label = 'Guardian' },
		  { name = 'burrito3',   label = 'Fourgonnette' },
		  { name = 'cog55',       label = 'Executive' },
		  { name = 'mesa',       label = 'Tout-Terrain' },		  
	  },

    Cloakrooms = {
      { x = 1009.98, y = -3167.97, z = -39.6 },
    },

    Armories = {
      { x = 1008.57, y = -3171.15, z = -39.6 },
    },

    Vehicles = {
      {
        Spawner    = { x = -77.38, y = 363.98, z = 111.5 },
        SpawnPoint = { x = -74.08, y = 357.84, z = 112.44 },
        Heading    = 241.3,
      }
    },
	
	Helicopters = {
      {                          
        Spawner    = { x = 27.21, y = -919.9, z = -2.08 },
        SpawnPoint = { x = 27.21, y = -919.9, z = -2.08 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -80.0, y = 369.11, z = 111.5 }
    },

    BossActions = {
      { x = 1009.77, y = -3164.14, z = -34.9 }
    },

  },

}

Config.green 				  = 681014
Config.grey 				  = 8421504
Config.red 					  = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.yelow				  = 15921158
Config.purple 				  = 11750815
Config.phone 				  = 12654922
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"