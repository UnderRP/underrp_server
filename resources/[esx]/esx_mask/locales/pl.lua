Locales ['pl'] = {
    ['valid_purchase'] = 'czy napewno chcesz to zakupić?',
    ['yes'] = 'tak',
    ['no'] = 'nie',
    ['not_enough_money'] = 'nie posiadasz wystarczająco pieniędzy',
    ['press_access'] = 'wciśnij ~INPUT_CONTEXT~ aby otworzyć menu',
    ['masks_blip'] = 'maski',
    ['no_mask'] = 'nie posiadasz maski',
    ['you_paid'] = 'zapłaciłeś/aś ',
}