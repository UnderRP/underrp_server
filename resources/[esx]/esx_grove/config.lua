Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 2.0, y = 2.0, z = 1.0 }
Config.MarkerColor                = { r = 0, g = 255, b = 0 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale = 'en'

Config.GroveStations = {

  Grove = {

    Blip = {
   -- Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_COMBATPISTOL',     price = 20000 },
      { name = 'WEAPON_MICROSMG',       price = 125000 },
      { name = 'WEAPON_ASSAULTRIFLE',     price = 400000 },
   --   { name = 'WEAPON_PUMPSHOTGUN',      price = 18000 },
      { name = 'WEAPON_STUNGUN',          price = 2500 },
      { name = 'WEAPON_FLASHLIGHT',       price = 500 },
   --   { name = 'WEAPON_FIREEXTINGUISHER', price = 50 },
   --   { name = 'WEAPON_CARBINERIFLE',     price = 50000 },
   --   { name = 'WEAPON_ADVANCEDRIFLE',    price = 50000 },
   --   { name = 'WEAPON_SNIPERRIFLE',      price = 150000 },
   --   { name = 'WEAPON_SMOKEGRENADE',     price = 8000 },
      { name = 'WEAPON_VINTAGEPISTOL',         price = 60000 },
      { name = 'WEAPON_MACHINEPISTOL',         price = 90000 },
   --   { name = 'WEAPON_FLARE',            price = 8000 },
      { name = 'WEAPON_KNUCKLE',      price = 500 },
	  { name = 'WEAPON_BAT',          price = 500 },
	  
    },

	  AuthorizedVehicles = {
		  { name = 'voodoo',      label = 'Voodoo' },
		  { name = 'sandking',   label = '4X4' },
		  { name = 'mule3',      label = 'Camion de Transport' },
		  { name = 'guardian',   label = 'Guardian' },
		  { name = 'burrito3',   label = 'Fourgonnette' },
		  { name = 'cog55',       label = 'Executive' },
		  { name = 'pcs18',       label = 'Porsche Cayenne' },		  
	  },

    Cloakrooms = {
      { x = 9.283, y = 528.914, z = 169.635 },
    },

    Armories = {
      { x = 1.550, y = 527.397, z = 170.017 },
    },

    Vehicles = {
      {
        Spawner    = { x = 13.40, y = 549.1, z = 175.187 },
        SpawnPoint = { x = 8.237, y = 556.963, z = 175.266 },
        Heading    = 90.0,
      }
    },
	
	Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 22.74, y = 545.9, z = 175.027 },
      { x = 21.35, y = 543.3, z = 175.027 },
    },

    BossActions = {
      { x = 4.113, y = 526.897, z = 173.628 }
    },

  },

}

Config.green 				  = 681014
Config.grey 				  = 8421504
Config.red 					  = 16711680
Config.orange 				  = 16744192
Config.blue 				  = 2061822
Config.yelow				  = 15921158
Config.purple 				  = 11750815
Config.phone 				  = 12654922
Config.webhook                = "https://discordapp.com/api/webhooks/531525521578655806/il4wBG3Sx4iec4_75p6NnE19jXeM8puTfsdTYabOMRwFGQ_uv5tFr5-huhWmEdf6AMVp"
